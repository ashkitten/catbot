import os, json, random, re
from mastodon import Mastodon
from apscheduler.schedulers.blocking import BlockingScheduler

def flatten_to_list(d):
    items = []
    for k, v in d.items():
        if type(v) is dict:
            items.extend(flatten_to_list(v))
        elif type(v) is list:
            items.extend(v)
        else:
            items.append(v)
    return items

def get_random_text():
    toots = json.load(open(os.path.join(os.path.dirname(__file__), "toots.json")))
    choice = random.choice(toots["phrases"])
    text = choice[0]
    while(True):
        match = re.search("%(\d)", text)
        if not match:
            break

        options = []
        for i in choice[int(match.group(1))]:
            val = toots["replacements"]
            for j in i.replace(" ", "").split(">"):
                try:
                    val = val[j]
                except KeyError:
                    print("Invalid key: \"" + i + "\"")
            if type(val) is dict:
                options.extend(flatten_to_list(val))
            else:
                options.extend(val)

        text = text.replace(match.group(0), random.choice(options))

    # Remove extra space from "misc > empty" and beginning/end
    text = text.replace("  ", " ").strip()
    # Remove extra space from end of action
    text = text.replace(" *", "*")

    return text

def toot_random():
    mastodon.toot(get_random_text())

if __name__ == "__main__":
    mastodon = Mastodon(
        client_id = "clientcred.txt",
        access_token = "usercred.txt",
        api_base_url = "https://botsin.space"
    )

    scheduler = BlockingScheduler()
    scheduler.add_job(toot_random, 'cron', hour = "*")
    scheduler.start()
